/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./worker.js":
/*!*******************!*\
  !*** ./worker.js ***!
  \*******************/
/***/ (() => {

eval("function sendDataToServer(data) {\n  var xhr = new XMLHttpRequest();\n  xhr.open('post', 'api//analytics/user', true);\n  xhr.responseType = 'json';\n  xhr.setRequestHeader('Content-Type', 'Application/json');\n\n  xhr.onerror = function (error) {\n    console.log(error);\n  };\n\n  xhr.send(data);\n}\n\nvar batch = [];\n\nonmessage = function handleMessageFromJoinUsSection(msg) {\n  var receivedButtonClicks = msg.data;\n  console.log('received data'); // eslint-disable-next-line no-plusplus\n\n  for (var i = 0; i < 1; i++) {\n    batch.push(receivedButtonClicks);\n\n    if (batch.length > 4) {\n      sendDataToServer(batch);\n      batch = [];\n    }\n  }\n};\n\n//# sourceURL=webpack:///./worker.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./worker.js"]();
/******/ 	
/******/ })()
;