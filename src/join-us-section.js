import validate from './email-validator';

const myWorker = new Worker('./worker.bundle.js');

const learnMoreSection = document.querySelector('.app-section.app-section--image-culture');

function initializeEmailForm(buttonText) {
  const form = document.querySelector('#email_form');
  form.innerHTML = ` <input required type="email">
                       <button id="email_button"  type="submit" class="app-section__button app-section__button--read-more">${buttonText}</button>
                  `;

  const emailInput = document.querySelector('.join_our_program form input');
  emailInput.style.backgroundColor = 'rgba(255, 255, 255, 0.1)';
  emailInput.style.border = 'none';
  emailInput.style.width = '400px';
  emailInput.style.height = '36px';
  emailInput.style.paddingLeft = '20px';
  emailInput.style.paddingTop = '5px';
  emailInput.style.paddingBottom = '4px';
  emailInput.placeholder = localStorage.getItem('email') || 'Email';
  emailInput.style.color = 'rgb(255, 255, 255)';
  emailInput.style.marginRight = '30px';
}

function modifyEmailForm() {
  const form = document.querySelector('#email_form');
  form.innerHTML = '<button id="email_button"  type="submit" class="app-section__button app-section__button--read-more">UNSUBSCRIBE</button>';
}

function requestSubscribe(email) {
  myWorker.postMessage(['subscribed']);
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/subscribe', true);
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');

  const button = document.querySelector('#email_button');
  button.setAttribute('disabled', true);
  button.style.opacity = 0.5;

  xhr.onload = () => {
    button.removeAttribute('disabled');
    button.style.opacity = 1;
    if (xhr.status === 422) {
      /* eslint-disable */
      window.alert(JSON.stringify(xhr.response));
    } else {
      console.log(xhr.response);
      localStorage.setItem('email', email);
      modifyEmailForm();
    }
  };

  xhr.onerror = (error) => { console.log(error); };
  xhr.send(JSON.stringify({ email }));
}

function requestUnsubscribe(buttonText) {
  myWorker.postMessage(['unsubscribed']);
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/unsubscribe', true);
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');

  const button = document.querySelector('#email_button');
  button.setAttribute('disabled', true);
  button.style.opacity = 0.5;

  xhr.onload = () => {
    button.removeAttribute('disabled');
    button.style.opacity = 1;
    localStorage.removeItem('email');
    initializeEmailForm(buttonText);
  };

  xhr.onerror = (error) => { console.log(error); };
  xhr.send();
}

function createJoinUsSection(headline, buttonText) {
  const joinUsSection = document.createElement('section');
  joinUsSection.id = 'join_our_product_section';
  joinUsSection.className = 'app-section join_our_program';
  joinUsSection.style.maxWidth = '1200px';
  joinUsSection.style.color = 'white';
  joinUsSection.style.alingItems = 'center';
  joinUsSection.style.height = '436px';
  joinUsSection.style.background = 'linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), URL(assets/images/join-our-team-large.webp)';

  const content = `  
                    <h1 class="app-title">${headline}</h1>
                    <h2 class="app-subtitle">Sed do eiusmod tempor incididunt </br>
                                ut labore et dolore magna aliqua.</h2>
                    <form id="email_form"> 
                    </form>
                `;

  joinUsSection.innerHTML = content;
  learnMoreSection.after(joinUsSection);
  initializeEmailForm(buttonText);

  const form = document.querySelector('#email_form');
  form.addEventListener('submit', (e) => {
    e.preventDefault();

    const emailInput = document.querySelector('.join_our_program form input');
    const button = document.querySelector('#email_button');

    if (button.innerHTML === 'SUBSCRIBE') {
      const email = emailInput.value;
      const isValid = validate(email);
      if (isValid) {
        requestSubscribe(email);
      } else {
        /* eslint-disable */
        emailInput.value = '';
      }
    } else {
      requestUnsubscribe(buttonText);
    }
  });
}

class SectionCreator {
  create(type) {
    switch (type) {
      case 'standard':
        createJoinUsSection('Join Our Program', 'SUBSCRIBE');
        break;
      case 'advanced':
        createJoinUsSection('Join Our Advanced Program', 'Subscribe to Advanced Program');
    }
  }

  remove() {
    setTimeout(() => {
      const element = document.getElementById('join_our_product_section');
      element.remove();
    }, 1000);
  }
}

export default SectionCreator;
