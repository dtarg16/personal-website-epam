const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yahoo.com'];

function validate(email) {
  const domain = email.substring(email.lastIndexOf('@') + 1);

  return VALID_EMAIL_ENDINGS.includes(domain);
}

async function validateAsync(email) {
  const domain = email.substring(email.lastIndexOf('@') + 1);

  return VALID_EMAIL_ENDINGS.includes(domain);
}

function validateWithThrow(email) {
  const domain = email.substring(email.lastIndexOf('@') + 1);

  if (!VALID_EMAIL_ENDINGS.includes(domain)) {
    throw new Error('Invalid email address');
  }

  return true;
}

function validateWithLog(email) {
  const domain = email.substring(email.lastIndexOf('@') + 1);

  const result = VALID_EMAIL_ENDINGS.includes(domain);

  console.log(result);
  return result;
}

export default validate;
export {
  validateAsync, validateWithThrow, validateWithLog,
};
