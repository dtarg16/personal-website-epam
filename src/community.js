function loadCommunity() {
  performance.mark('communityStart');
  const firstUserImg = document.querySelector('.first-user img');
  const firstUserUsername = document.querySelector('.first-user .username');
  const firstUserPosition = document.querySelector('.first-user .position');

  const secondUserImg = document.querySelector('.second-user img');
  const secondUserUsername = document.querySelector('.second-user .username');
  const secondUserPosition = document.querySelector('.second-user .position');

  const thirdUserImg = document.querySelector('.third-user img');
  const thirdUserUsername = document.querySelector('.third-user .username');
  const thirdUserPosition = document.querySelector('.third-user .position');

  const xhr = new XMLHttpRequest();
  xhr.open('get', 'api/community', true);
  xhr.onload = () => {
    performance.mark('communityEnd');
    performance.measure('community', 'communityStart', 'communityEnd');
    const data = JSON.parse(xhr.response);

    const firstUser = data[0];
    firstUserImg.src = firstUser.avatar;
    firstUserUsername.innerHTML = `${firstUser.firstName} ${firstUser.lastName}`;
    firstUserPosition.innerHTML = `${firstUser.position}`;

    const secondUser = data[1];
    secondUserImg.src = secondUser.avatar;
    secondUserUsername.innerHTML = `${secondUser.firstName} ${secondUser.lastName}`;
    secondUserPosition.innerHTML = `${secondUser.position}`;

    const thirdUser = data[2];
    thirdUserImg.src = thirdUser.avatar;
    thirdUserUsername.innerHTML = `${thirdUser.firstName} ${thirdUser.lastName}`;
    thirdUserPosition.innerHTML = `${thirdUser.position}`;
  };
  xhr.send();
}

export default loadCommunity;
