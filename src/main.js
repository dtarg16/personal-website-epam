import './styles/style.css';
import SectionCreator from './join-us-section';
import loadCommunity from './community';

const sectionCreator = new SectionCreator();

function perf(type, name, data, options = '') {
  console.log(
    `%c${type}: %c${name} | %c${data ? `${Math.round(data)}ms` : ''}  %c${options}`,
    'color:blue',
    'color:green',
    'color:gray',
    'color:lightblue',
  );
}

function sendPerformanceRequest(payload) {
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/analytics/performance', true);
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');
  xhr.send(JSON.stringify(payload));
}

window.addEventListener('load', () => {
  sectionCreator.create('standard');
  loadCommunity();

  const navEntries = performance.getEntriesByType('navigation');
  navEntries.forEach((entry) => {
    sendPerformanceRequest(entry);
    perf('navigation', 'fetch-start', entry.fetchStart);
    const ttfb = entry.responseStart - entry.fetchStart;
    perf('memory', 'ttfb', ttfb);
  });

  const resEntries = performance.getEntriesByType('resource');
  resEntries.forEach((entry) => {
    const size = `${Math.round(entry.encodedBodySize / 1024)}Kb`;
    const ttfb = entry.responseStart - entry.fetchStart;
    perf(entry.initiatorType, entry.name, ttfb, size);
  });

  const userObserver = new PerformanceObserver((list) => {
    list.getEntries().forEach((entry) => {
      sendPerformanceRequest(entry);
      perf(
        entry.entryType,
        entry.name,
        entry.entryType === 'mark' ? entry.startTime : entry.duration,
      );
    });
  });
  userObserver.observe({ entryTypes: ['mark', 'measure'] });

  const paintObserver = new PerformanceObserver((list) => {
    const firstPaint = list.getEntriesByName('first-paint');
    if (firstPaint.length > 0) {
      perf('Paint', 'First Paint', firstPaint[0].startTime);
    }

    const firstContenfulPaint = list.getEntriesByName('first-contentful-paint');
    if (firstContenfulPaint.length > 0) {
      perf('Paint', 'First Contentful Paint', firstContenfulPaint[0].startTime);
    }
    sendPerformanceRequest(firstPaint);
    sendPerformanceRequest(firstContenfulPaint);
  });

  try {
    paintObserver.observe({ entryTypes: ['paint'] });
  } catch (e) {
    console.log('Paint Timing API not available');
  }
});
