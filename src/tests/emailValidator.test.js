import { expect } from 'chai';
import { describe, it } from 'mocha';
import sinon from 'sinon';
// eslint-disable-next-line import/extensions
import validate, { validateAsync, validateWithLog, validateWithThrow } from '../email-validator.js';

describe('test custom email validator', () => {
  it('it should return true: gmail', () => {
    const expected = true;
    const actual = validate('validEmail@gmail.com');
    expect(actual).to.equal(expected);
  });

  it('it should return true: yahoo', () => {
    const expected = true;
    const actual = validate('validEmail@yahoo.com');
    expect(actual).to.equal(expected);
  });

  it('it should return true: outlook', () => {
    const expected = true;
    const actual = validate('validEmail@outlook.com');
    expect(actual).to.equal(expected);
  });

  it('it should return false: no @', () => {
    const expected = false;
    const actual = validate('validEmail.gmail.com');
    expect(actual).to.equal(expected);
  });

  it('it should return false: wrong ending', () => {
    const expected = false;
    const actual = validate('validEmail@ggmail.com');
    expect(actual).to.equal(expected);
  });

  it('it should return false: wrong ending', () => {
    const expected = false;
    const actual = validate('validEmail@gmail.com1');
    expect(actual).to.equal(expected);
  });
});

describe('test custom async email validator', () => {
  it('it should return true: gmail', async () => validateAsync('validEmail@gmail.com').then((actual) => {
    const expected = true;
    expect(actual).to.equal(expected);
  }));

  it('it should return true: yahoo', async () => validateAsync('validEmail@yahoo.com').then((actual) => {
    const expected = true;
    expect(actual).to.equal(expected);
  }));

  it('it should return true: outlook', async () => validateAsync('validEmail@yahoo.com').then((actual) => {
    const expected = true;
    expect(actual).to.equal(expected);
  }));

  it('it should return false: no @', async () => validateAsync('validEmail.gmail.com').then((actual) => {
    const expected = false;
    expect(actual).to.equal(expected);
  }));

  it('it should return false: wrong ending', async () => validateAsync('validEmail@ggmail.com').then((actual) => {
    const expected = false;
    expect(actual).to.equal(expected);
  }));

  it('it should return false: wrong ending', async () => validateAsync('validEmail@gmail.com1').then((actual) => {
    const expected = false;
    expect(actual).to.equal(expected);
  }));
});

describe('test custom email validator throw error', () => {
  const errorMessage = 'Invalid email address';

  it('it should return true: gmail', () => {
    const expected = true;
    const actual = validateWithThrow('validEmail@gmail.com');
    expect(actual).to.equal(expected);
  });

  it('it should return true: yahoo', () => {
    const expected = true;
    const actual = validateWithThrow('validEmail@yahoo.com');
    expect(actual).to.equal(expected);
  });

  it('it should return true: outlook', () => {
    const expected = true;
    const actual = validateWithThrow('validEmail@outlook.com');
    expect(actual).to.equal(expected);
  });

  it('it should throw an error : no @', () => {
    expect(() => {
      validateWithThrow('validEmail.gmail.com');
    }).to.throw(Error, errorMessage);
  });

  it('it should throw an error: wrong ending', () => {
    expect(() => {
      validateWithThrow('validEmail@ggmail.com');
    }).to.throw(Error, errorMessage);
  });

  it('it should return false: wrong ending', () => {
    expect(() => {
      validateWithThrow('validEmail@gmail.com1');
    }).to.throw(Error, errorMessage);
  });
});

describe('test custom email validator with console log in it', () => {
  beforeEach(() => {
    sinon.spy(console, 'log');
  });

  afterEach(() => {
    console.log.restore();
  });

  it('it should log true: gmail', () => {
    const expected = true;
    const actual = validateWithLog('validEmail@gmail.com');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(true)).to.be.true;
    expect(actual).to.equal(expected);
  });

  it('it should log true: yahoo', () => {
    const expected = true;
    const actual = validateWithLog('validEmail@yahoo.com');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(true)).to.be.true;
    expect(actual).to.equal(expected);
  });

  it('it should log true: outlook', () => {
    const expected = true;
    const actual = validateWithLog('validEmail@outlook.com');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(true)).to.be.true;
    expect(actual).to.equal(expected);
  });

  it('it should log false: no @', () => {
    const expected = false;
    const actual = validateWithLog('validEmail.gmail.com');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(false)).to.be.true;
    expect(actual).to.equal(expected);
  });

  it('it should log false: wrong ending', () => {
    const expected = false;
    const actual = validateWithLog('validEmail@ggmail.com');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(false)).to.be.true;
    expect(actual).to.equal(expected);
  });

  it('it should log false: wrong ending', () => {
    const expected = false;
    const actual = validateWithLog('validEmail@gmail.com1');
    expect(console.log.calledOnce).to.be.true;
    expect(console.log.calledWith(false)).to.be.true;
    expect(actual).to.equal(expected);
  });
});
