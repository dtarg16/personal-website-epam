function sendDataToServer(data) {
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/analytics/user', true);
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');
  xhr.onerror = (error) => { console.log(error); };

  xhr.send(JSON.stringify(data));
}

let batch = [];
onmessage = function handleMessageFromJoinUsSection(msg) {
  const receivedButtonClicks = msg.data;
  // eslint-disable-next-line no-plusplus
  batch.push(receivedButtonClicks);
  console.log(batch);
  if (batch.length > 4) {
    sendDataToServer(batch);
    batch = [];
  }
};
