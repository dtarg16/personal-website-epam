module.exports = {
  env: {
    browser: true,
    es2021: true,
    mocha: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'chai-friendly',
  ],
  rules: {
    'comma-spacing': ['error', { before: false, after: true }],
    'no-unused-vars': ['error', {
      args: 'none',
      caughtErrors: 'none',
      ignoreRestSiblings: true,
      vars: 'all',
    }],
    'no-useless-rename': 'error',
    'no-useless-return': 'error',
    'no-console': 'off',
    'class-methods-use-this': 'off',
    'default-case': 'off',
    'linebreak-style': 'off',
    'no-unused-expressions': 'off',
  },
};
